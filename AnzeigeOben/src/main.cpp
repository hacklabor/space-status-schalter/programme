#include <Arduino.h>
#include <Wire.h>
#include <Wire.h>
#include <IS31FL3730.h>

#define M1 0
#define M2 1
#define SDA_1 27
#define SCL_1 26

#define SDA_2 32
#define SCL_2 33

TwoWire I2Cone = TwoWire(0);
TwoWire I2Ctwo = TwoWire(1);

IS31FL3730 m1;
IS31FL3730 m2;
IS31FL3730 m3;

void clear_matrix(IS31FL3730 m)
{
  for (uint8_t y = 0; y < 7; y++)
  {
    for (uint8_t x = 0; x < 5; x++)
    {
      m.set_pixel(M1, x, y, 0);
      m.set_pixel(M2, x, y, 0);
   
    }
  }
  m.set_decimal(M1, 0);
  m.set_decimal(M2, 0);
  m.update();
  delay(10);
}

void fill_matrix(IS31FL3730 m)
{
  for (uint8_t y = 0; y < 7; y++)
  {
    for (uint8_t x = 0; x < 5; x++)
    {
      m.set_pixel(M1, x, y, 1);
      m.set_pixel(M2, x, y, 1);
      
      //delay(1);
    }
    
  }
  m.set_decimal(M1, 1);
  m.set_decimal(M2, 1);
  m.update();
  delay(10);
}

void setup()
{
  Serial.begin(9600);
  while (!Serial)
  {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.println("ISSI test");
  pinMode(SDA_1, INPUT_PULLUP);
  pinMode(SCL_1, INPUT_PULLUP);
  pinMode(SDA_2, INPUT_PULLUP);
  pinMode(SCL_2, INPUT_PULLUP);

  m1.begin(0x61, SDA_1, SCL_1);
  m2.begin(0x62, SDA_1, SCL_1);
  m3.begin(0x63, SDA_1, SCL_1);

  // set Lighting Effect Register to 10mA
  m1.set_le(0x9);
  m1.set_bright(10);
  m2.set_bright(10);
  m3.set_bright(10);

  m1.clear();
  m2.clear();
  m3.clear();
}

void loop()
{
  fill_matrix(m1);
  fill_matrix(m2);
  fill_matrix(m3);
  delay(10000);
  clear_matrix(m1);
  clear_matrix(m2);
  clear_matrix(m3);
  delay(1000);
}







