#include "arduino.h"
#include "motorstepper.h"

motorstepper::motorstepper(int _PIN_PWM, int _PIN_DIR, int _PIN_ENABLE, int _PIN_END_OPEN, int _PIN_END_CLOSE, unsigned long _maxMoveTime)
{
    PIN_PWM = _PIN_PWM;
    PIN_DIR = _PIN_DIR;
    PIN_ENABLE = _PIN_ENABLE;
    maxMoveTime = _maxMoveTime;
    PIN_END_OPEN = _PIN_END_OPEN;
    PIN_END_CLOSE = _PIN_END_CLOSE;
    pinMode(PIN_ENABLE, OUTPUT);
   
    pinMode(PIN_DIR, OUTPUT);
    pinMode(PIN_END_OPEN, INPUT_PULLUP);
    pinMode(PIN_END_CLOSE, INPUT_PULLUP);
}




    



void motorstepper::start(int _Power, int _direction)
{

    if (MotorRun == true){
        return;
    }
    direction = _direction;
    digitalWrite(PIN_ENABLE, false);

    if (direction == 0)
    {

        digitalWrite(PIN_DIR, 0);
            
    
            }
            else
            {
            digitalWrite(PIN_DIR, 1);
            
            }
            analogWrite(PIN_PWM, _Power);
            StartTime = millis();
            MotorRun = true;
}

bool motorstepper::checkPosSwitch(){
           
           if (millis()-StartTime>maxMoveTime){
            analogWrite(PIN_PWM, 0);
            StartTime = millis();
            Serial.println("StopMotor Timeout");
            MotorRun = false;
           }
        
            Serial.println(digitalRead(PIN_END_OPEN));
            if (digitalRead(PIN_END_OPEN) == HIGH && !direction)
            {
                Serial.println("StopMotor OPEN");
                delay(250);
                analogWrite(PIN_PWM, 0);
                MotorRun = false;
        }



        if (digitalRead(PIN_END_CLOSE) == HIGH && direction)
        {   
            Serial.println("StopMotor Close");
            delay(250);
            analogWrite(PIN_PWM, 0);
            digitalWrite(PIN_ENABLE, true);
            MotorRun = false;
        }
        return MotorRun;
}
