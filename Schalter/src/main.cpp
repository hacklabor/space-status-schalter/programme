#include "motorstepper.h"
#include <PubSubClient.h>
#if defined(ESP8266)
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#elif defined(ESP32)
#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#endif
#include <ElegantOTA.h>
#include <secrets.h>
#if defined(ESP8266)
ESP8266WebServer server(80);
#elif defined(ESP32)
WebServer server(80);
#endif

enum SchalterState
{
  CLOSE = 1,
  OPEN = 2,
  NONE = 0,

};

int Dir_Open = 0;
int Dir_Close = 1;
int M1_enable = D5;
int M1_PWM = D6;
int M1_DIR = D7;
int M1_Open = D1;
int M1_Close = D2;
int oldState = NONE;
unsigned long M1_Timeout=8000;

long oldtime = 0;

String Serial_IN = "";

motorstepper m1(M1_PWM, M1_DIR, M1_enable, M1_Open, M1_Close,M1_Timeout);
// Wifi
WiFiClient espClient;
PubSubClient client(espClient);

void callback(char *topic, byte *payload, unsigned int length)
{

  //Serial.print("Message arrived [");
  //Serial.print(topic);
  //Serial.print("] ");
  
  String command;

  for (unsigned int i = 0; i < length; i++)
  {
   
    command = command + String((char)payload[i]);
   
  }
  Serial.println(command);
  if (command == "OPEN" && !digitalRead(M1_Open))
  {
    m1.start(100, Dir_Open);
  }

  if (command == "CLOSE" && !digitalRead(M1_Close))
  {
    m1.start(100, Dir_Close);
  }
}

  void reconnect()
  {
    // Loop until we're reconnected
    while (!client.connected())
    {
      Serial.print("Attempting MQTT connection...");
      // Attempt to connect
      if (client.connect(willClientID, mqtt_user, mqtt_password, willTopic, willQoS, willRetain, willOffMessage))
      {
        Serial.println("connected");

        // Once connected, publish an announcement...
        client.publish(willTopic, willOnMessage, willRetain);
        // ... and resubscribe
        client.subscribe(TopicRx);
      }
      else
      {
        Serial.print("failed, rc=");
        Serial.print(client.state());
        Serial.println(" try again in 5 seconds");
        // Wait 5 seconds before retrying
        delay(5000);
      }
    }
  }

  void Wifi_ElegantOTA()
  {
    WiFi.mode(WIFI_STA);
    WiFi.setHostname(STAHOST);
    WiFi.begin(STASSID, STAPSK);
    WiFi.setHostname(STAHOST);
    Serial.println("Start");
    while (WiFi.waitForConnectResult() != WL_CONNECTED)
    {
      Serial.println("Connection Failed! Rebooting...");
      delay(5000);
      ESP.restart();
    }

    server.on("/", []()
              { server.send(200, "text/plain", "Hi! I am SpaceStatusSchalter \n OTA Update unter http://[hostname]/update "); });

    ElegantOTA.begin(&server); // Start ElegantOTA
    server.begin();
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
  }

  void setup()
  {
    Serial.begin(115200);
    Wifi_ElegantOTA();
    // MQTT connect
    Serial.print("Connecting to MQTT...");
    // connecting to the mqtt server
    client.setServer(mqtt_server, 1883);
    client.setCallback(callback);
    Serial.println("done!");
    delay(1000);
    //m1.start(100,Dir_Close );
  }

  void loop()
  {

    server.handleClient();
    if (!client.connected())
    {
      reconnect();
    }
    client.loop();
    if (millis() - oldtime > 100)
    {
      bool run=m1.checkPosSwitch();
      if (digitalRead(M1_Close) && !run)
      {
        digitalWrite(M1_enable, true);
      }
      else
      {
        digitalWrite(M1_enable, false);
      }
      
      if (oldState==NONE && digitalRead(M1_Open)){
        oldState = OPEN;
        client.publish(TopicTx, "isOpen");
      }
      if (oldState == NONE && digitalRead(M1_Close))
      {
        oldState = CLOSE;
        client.publish(TopicTx, "isClose");
      }
      if (!digitalRead(M1_Close) && !digitalRead(M1_Open))
      {
        oldState = NONE;
        
      }
      
    }
  }
