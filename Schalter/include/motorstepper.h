#pragma once

#include <Arduino.h>


class motorstepper

{


    private:
int PIN_PWM;
int PIN_DIR;
int PIN_ENABLE;
int PIN_END_OPEN ;
int PIN_END_CLOSE ;

int direction = 0;
unsigned long StartTime=0;
unsigned long maxMoveTime=0;
bool MotorRun = false;



public:
    motorstepper(int _PIN_PWM, int _PIN_DIR, int _PIN_ENABLE, int _PIN_END_OPEN,int _PIN_END_CLOSE,unsigned long _maxMoveTime);


    void start(int _Power, int direction);
   
 bool checkPosSwitch();
};